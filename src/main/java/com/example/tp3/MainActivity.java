package com.example.tp3;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.tp3.data.Match;
import com.example.tp3.data.SportDbHelper;
import com.example.tp3.data.Team;
import com.example.tp3.webservice.JSONResponseHandlerMatch;
import com.example.tp3.webservice.JSONResponseHandlerRanking;
import com.example.tp3.webservice.JSONResponseHandlerTeam;
import com.example.tp3.webservice.WebServiceUrl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ListView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static com.example.tp3.data.Team.TAG;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static SportDbHelper dbHelper;
    public static SwipeRefreshLayout swi;
    public static RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.dbHelper = new SportDbHelper(this);
        this.deleteDatabase("sport.db");
        this.dbHelper.populate();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewTeams);
        recyclerView.setItemViewCacheSize(0);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        IconicAdapter adapter = new IconicAdapter();
        recyclerView.setAdapter(adapter);
        Log.d(SportDbHelper.class.getSimpleName(), "NB : "+adapter.getItemCount());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent switchToTeam = new Intent(view.getContext(), NewTeamActivity.class);
                NewTeamActivity.setMainActivity(MainActivity.this);
                MainActivity.this.startActivity(switchToTeam);
            }
        });

        swi = findViewById(R.id.swipe_refresh);
        swi.setOnRefreshListener(this);

        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int pos = viewHolder.getAdapterPosition();
                MainActivity.recyclerView.removeViewAt(pos);
                MainActivity.recyclerView.getAdapter().notifyItemRemoved(pos);
                MainActivity.dbHelper.deleteTeam(pos);
                Log.d(TAG, "delete : "+pos);
                IconicAdapter adapter = new IconicAdapter();
                MainActivity.recyclerView.setAdapter(adapter);
            }
        };
        new ItemTouchHelper(callback).attachToRecyclerView(MainActivity.recyclerView);
    }

    public Resources resources() {
        return this.getResources();
    }

    @Override
    public void onRefresh()
    {
        MyAllAsyncTask async = new MyAllAsyncTask(MainActivity.this, dbHelper.getAllTeams());
        async.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateView() {
        IconicAdapter adapter = new IconicAdapter();
        recyclerView.setAdapter(adapter);
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Log.d(SportDbHelper.class.getSimpleName(), "create row !");
            RowHolder rh = new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false), MainActivity.this);
            return rh;
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            final List teams = MainActivity.dbHelper.getAllTeams();
            final Team current = MainActivity.dbHelper.getTeam(position+1);
            if(current != null) {
                Log.d(SportDbHelper.class.getSimpleName(), "display: " + current.getName() + " with id=" + current.getId() + " and position=" + position);
                try {
                    holder.bindModel(current);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {
            return(MainActivity.dbHelper.getAllTeams().size());
        }

        public void updateView() {

        }
    }
}

class MyAllAsyncTask extends AsyncTask<Object, Void, Void> {
    private MainActivity activity;
    private List<Team> teams;

    // only retain a weak reference to the activity
    MyAllAsyncTask(MainActivity context, List<Team> teams) {
        activity = context;
        this.teams = teams;
    }


    @Override
    protected Void doInBackground(Object[] objects) {
        for (Team team:teams) {
            try {
                URL teamUrl = null;
                HttpURLConnection urlConnectionTeam = null;
                URL rankUrl = null;
                HttpURLConnection urlConnectionRank = null;
                URL matchUrl = null;
                HttpURLConnection urlConnectionMatch = null;

                Log.d(TAG, "Updating : " + team.getName());
                teamUrl = WebServiceUrl.buildSearchTeam(team.getName());
                urlConnectionTeam = (HttpURLConnection) teamUrl.openConnection();
                InputStream inTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                JSONResponseHandlerTeam jsonTeam = new JSONResponseHandlerTeam(team);
                jsonTeam.readJsonStream(inTeam);

                Match match = new Match();
                matchUrl = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnectionMatch = (HttpURLConnection) matchUrl.openConnection();
                InputStream inEvents = new BufferedInputStream(urlConnectionMatch.getInputStream());
                JSONResponseHandlerMatch jsonEvents = new JSONResponseHandlerMatch(match);
                Log.d(TAG, "Updating match : " + match.getLabel());
                team.setLastEvent(match);
                jsonEvents.readJsonStream(inEvents);

                rankUrl = WebServiceUrl.buildGetRanking(team.getIdLeague());
                urlConnectionRank = (HttpURLConnection) rankUrl.openConnection();
                InputStream inRank = new BufferedInputStream(urlConnectionRank.getInputStream());
                JSONResponseHandlerRanking jsonRank = new JSONResponseHandlerRanking(team);
                jsonRank.readJsonStream(inRank);

            } catch (IOException e) {
                Log.d(TAG, "Error while updating : " + team.getName());
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void onPostExecute(Void param) {
        super.onPostExecute(param);
        for (Team team:teams) {
            MainActivity.dbHelper.updateTeam(team);
        }
        activity.updateView();
        MainActivity.swi.setRefreshing(false);
    }
}