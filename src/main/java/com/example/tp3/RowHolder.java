package com.example.tp3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp3.R;
import com.example.tp3.data.SportDbHelper;
import com.example.tp3.data.Team;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView title;
    ImageView icon;
    LinearLayout line;
    TextView league;
    TextView event;
    MainActivity main;
    Team content;

    RowHolder(View row, MainActivity main) {
        super(row);
        this.title = (TextView) row.findViewById(R.id.title);
        this.league = (TextView) row.findViewById(R.id.teamLeague);
        this.event = (TextView) row.findViewById(R.id.event);
        this.icon = (ImageView) row.findViewById(R.id.image);
        this.line = (LinearLayout) row.findViewById(R.id.row);
        this.main = main;
        this.line.setOnClickListener(this);
    }

    void bindModel(Team team) throws MalformedURLException {
        Log.d(SportDbHelper.class.getSimpleName(), "binding : "+team.getName()+" with league="+team.getLeague()+" and event="+team.getLastEvent().toString());
        this.title.setText(team.getName());
        this.league.setText(team.getLeague());
        this.event.setText(team.getLastEvent().toString());
        if(team.getId() <= 9) {
            String id = Integer.toString((int) team.getId());
            int imgId = this.main.resources().getIdentifier("team" + id, "drawable", "com.example.tp3");
            this.icon.setImageResource(imgId);
        }
        Log.d(SportDbHelper.class.getSimpleName(), "binded : "+team.getName()+" with league="+league.getText()+" and event="+event.getText().toString());
        this.content = team;
        //Drawable image = LoadImageFromWebOperations("https://www.thesportsdb.com/images/media/team/badge/90p5p01536392092.png");
        //this.icon.setImageResource(image);
        //Log.d(SportDbHelper.class.getSimpleName(), "image : "+image);
    }

    @Override
    public void onClick(View view) {
        int position = this.getAdapterPosition();
        Intent switchToTeam = new Intent(view.getContext(), TeamActivity.class);
        switchToTeam.putExtra(Team.TAG, this.content);
        itemView.getContext().startActivity(switchToTeam);
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, url);
            return d;
        } catch (Exception e) {
            return null;
        }
    }

}
