package com.example.tp3;

import android.content.res.Resources;
import android.os.AsyncTask;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.tp3.data.Match;
import com.example.tp3.data.SportDbHelper;
import com.example.tp3.data.Team;
import com.example.tp3.webservice.JSONResponseHandlerMatch;
import com.example.tp3.webservice.JSONResponseHandlerRanking;
import com.example.tp3.webservice.JSONResponseHandlerTeam;
import com.example.tp3.webservice.WebServiceUrl;

import static com.example.tp3.data.Team.TAG;
//import com.example.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        //this.team = (Team) getIntent().getExtras().get("team");
        this.team = (Team) getIntent().getParcelableExtra(Team.TAG);
        Log.d(SportDbHelper.class.getSimpleName(), "Intent : "+team.getName());

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        try {
            but.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Background task : Updating "+team.getName());
                    AsyncTask at = new MyAsyncTask(TeamActivity.this, team);
                    at.execute();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }

    void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());
        if (team.getId() <= 9) {
            String id = Integer.toString((int) team.getId());
            int imgId = resources().getIdentifier("team" + id, "drawable", "com.example.tp3");
            imageBadge.setImageResource(imgId);
        }
    }

    public Resources resources() {
        return this.getResources();
    }

}

class MyAsyncTask extends AsyncTask<Object, Void, Void> {
    private TeamActivity activity;
    private Team teamAS;
    private Match matchAS;

        // only retain a weak reference to the activity
        MyAsyncTask(TeamActivity context, Team team) {
            activity = context;
            teamAS = team;
            matchAS = new Match();
        }


    @Override
    protected Void doInBackground(Object[] objects) {
        URL teamUrl = null;
        HttpURLConnection urlConnectionTeam = null;
        URL rankUrl = null;
        HttpURLConnection urlConnectionRank = null;
        URL matchUrl = null;
        HttpURLConnection urlConnectionMatch = null;
        Log.d(TAG, "Background task : Updating "+teamAS.getName());
        try {
            Log.d(TAG, "Updating : "+teamAS.getName());
            teamUrl = WebServiceUrl.buildSearchTeam(teamAS.getName());
            urlConnectionTeam = (HttpURLConnection) teamUrl.openConnection();
            InputStream inTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
            JSONResponseHandlerTeam jsonTeam = new JSONResponseHandlerTeam(teamAS);
            jsonTeam.readJsonStream(inTeam);

            matchUrl = WebServiceUrl.buildSearchLastEvents(teamAS.getIdTeam());
            urlConnectionMatch = (HttpURLConnection) matchUrl.openConnection();
            InputStream inEvents = new BufferedInputStream(urlConnectionMatch.getInputStream());
            JSONResponseHandlerMatch jsonEvents = new JSONResponseHandlerMatch(matchAS);
            Log.d(TAG, "Updating match : "+matchAS.getLabel());
            teamAS.setLastEvent(matchAS);
            jsonEvents.readJsonStream(inEvents);

            rankUrl = WebServiceUrl.buildGetRanking(teamAS.getIdLeague());
            urlConnectionRank = (HttpURLConnection) rankUrl.openConnection();
            InputStream inRank = new BufferedInputStream(urlConnectionRank.getInputStream());
            JSONResponseHandlerRanking jsonRank = new JSONResponseHandlerRanking(teamAS);
            jsonRank.readJsonStream(inRank);

        } catch (IOException e) {
            Log.d(TAG, "Error while updating : "+teamAS.getName());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute(Void param) {
        Log.d(TAG, "Updating view for : "+teamAS.getName());
        activity.updateView();
        MainActivity.dbHelper.updateTeam(teamAS);
    }
}