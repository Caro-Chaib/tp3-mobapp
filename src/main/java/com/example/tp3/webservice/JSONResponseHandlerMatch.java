package com.example.tp3.webservice;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.example.tp3.data.Match;
import com.example.tp3.data.Team;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerMatch {

    private static final String TAG = JSONResponseHandlerMatch.class.getSimpleName();

    private Match match;


    public JSONResponseHandlerMatch(Match match) {
        this.match = match;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readMatchs(reader);
        } finally {
            reader.close();
        }
    }

    public void readMatchs(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                Log.d(TAG, "Results : ");
                readArrayMatchs(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayMatchs(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb>=0) {
                    if (name.equals("idEvent")) {
                        match.setId(reader.nextLong());
                        Log.d(TAG, "Last event : "+match.getId());
                    } else if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")) {
                        JsonToken peek = reader.peek();
                        if (peek == JsonToken.NULL) {
                            match.setHomeScore(0);
                            reader.skipValue();
                        }
                        else match.setHomeScore(reader.nextInt());
                    } else if (name.equals("intAwayScore")) {
                        JsonToken peek = reader.peek();
                        if (peek == JsonToken.NULL) {
                            match.setAwayScore(0);
                            reader.skipValue();
                        }
                        else match.setAwayScore(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
