package com.example.tp3.webservice;

import android.util.JsonReader;
import android.util.Log;

import com.example.tp3.data.Team;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=4414&s=1920
 * Responses must be provided in JSON.
 *
 */

public class JSONResponseHandlerRanking {

    private static final String TAG = JSONResponseHandlerRanking.class.getSimpleName();

    private Team team;
    int finaln;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        //JsonReader reader2 = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRankings(reader);
            //readTotals(reader2);
        } finally {
            reader.close();
            //reader2.close();
        }
    }

    public void readRankings(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayRankings(reader);
            } else {
                reader.skipValue();
            }
        }
        Log.d(TAG, "call readArrayTotal next ");
        reader.endObject();
    }

    public void readTotals(JsonReader reader) throws IOException {
        reader.beginObject();
        Log.d(TAG, "call readArrayTotal "+reader.hasNext());
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                Log.d(TAG, "call readArrayTotal");
                readArrayTotal(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayRankings(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        finaln = 0;
        while (reader.hasNext() ) {
            Log.d(TAG, "nb = "+nb);
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb>=0) {
                    if (name.equals("teamid")) {
                        long id = reader.nextLong();
                        if (id == team.getIdTeam()) {
                            team.setIdTeam(id);
                            team.setRanking(nb+1);
                            finaln = nb+1;
                        }
                    }
                    else if (name.equals("total") && finaln!=0) {
                        team.setTotalPoints(reader.nextInt());
                        finaln = 0;
                    }
                    else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

    private void readArrayTotal(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        Log.d(TAG, "nb = "+nb);
        while (reader.hasNext() ) {
            Log.d(TAG, "nb = "+nb);
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    Log.d(TAG, "final : "+finaln);
                    if (name.equals("total")) {
                        team.setTotalPoints(reader.nextInt());
                    }
                    else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
